<!DOCTYPE html> 
<html lang="en"> 
    <head> 
        <meta charset="UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="X-UA-Compatible" content="ie=edge"> 
        <title>Function</title> 
    </head> 
    <body> 
        <?php
            require_once 'animal.php';
            require_once 'frog.php';
            require_once 'ape.php';

            $sheep = new Animal("Shaun");
            echo "Name : " . $sheep->get_name() . "<br>";
            echo "Legs : " . $sheep->get_legs() . "<br>";
            echo "Cold Blooded : " . $sheep->get_cold_blooded() . "<br><br>";

            $kodok = new Frog("Buduk");
            echo "Name : " . $kodok->get_name() . "<br>";
            echo "Legs : " . $kodok->get_legs() . "<br>";
            echo "Cold Blooded : " . $kodok->get_cold_blooded() . "<br>";
            $kodok->jump();
            echo "<br>";
            
            $sungokong = new Ape("Kera Sakti");
            echo "Name : " . $sungokong->get_name() . "<br>";
            echo "Legs : " . $sungokong->get_legs() . "<br>";
            echo "Cold Blooded : " . $sungokong->get_cold_blooded() . "<br>";
            $sungokong->yell();
        ?>
    </body>
</html>