<!-- resources/views/casts/edit.blade.php -->
@extends('layouts.app')

@section('content')
    <h1>Edit Pemain Film</h1>
    <form action="{{ route('cast.update', $cast->id) }}" method="POST">
        @csrf
        @method('PUT')
        <label for="nama">Nama:</label>
        <input type="text" name="nama" value="{{ $cast->nama }}" required>
        <label for="deskripsi">Deskripsi:</label>
        <textarea name="deskripsi" required>{{ $cast->deskripsi }}</textarea>
        <button type="submit">Simpan Perubahan</button>
    </form>
    <a href="{{ route('cast.index') }}">Kembali ke List Pemain Film</a>
@endsection