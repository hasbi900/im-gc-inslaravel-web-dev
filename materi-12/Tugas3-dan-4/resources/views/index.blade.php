<!-- resources/views/casts/index.blade.php -->
@extends('layouts.app')

@section('content')
    <h1>List Data Pemain Film</h1>
    <table>
        <thead>
            <tr>
                <th>Nama</th>
                <th>Deskripsi</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($casts as $cast)
                <tr>
                    <td>{{ $cast->nama }}</td>
                    <td>{{ $cast->deskripsi }}</td>
                    <td>
                        <a href="{{ route('cast.show', $cast->id) }}">Show</a>
                        <a href="{{ route('cast.edit', $cast->id) }}">Edit</a>
                        <form action="{{ route('cast.destroy', $cast->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a href="{{ route('cast.create') }}">Tambah Pemain Film Baru</a>
@endsection