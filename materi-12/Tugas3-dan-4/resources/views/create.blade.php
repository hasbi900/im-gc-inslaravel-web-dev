<!-- resources/views/casts/create.blade.php -->
@extends('layouts.app')

@section('content')
    <h1>Form Tambah Pemain Film</h1>
    <form action="{{ route('cast.store') }}" method="POST">
        @csrf
        <label for="nama">Nama:</label>
        <input type="text" name="nama" required>
        <label for="deskripsi">Deskripsi:</label>
        <textarea name="deskripsi" required></textarea>
        <button type="submit">Simpan</button>
    </form>
    <a href="{{ route('cast.index') }}">Kembali ke List Pemain Film</a>
@endsection