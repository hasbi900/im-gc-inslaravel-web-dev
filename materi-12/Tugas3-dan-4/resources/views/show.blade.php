<!-- resources/views/casts/show.blade.php -->
@extends('layouts.app')

@section('content')
    <h1>Detail Pemain Film</h1>
    <p><strong>Nama:</strong> {{ $cast->nama }}</p>
    <p><strong>Deskripsi:</strong> {{ $cast->deskripsi }}</p>
    <a href="{{ route('cast.index') }}">Kembali ke List Pemain Film</a>
@endsection