<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

use App\Http\Controllers\Controller;
use App\Http\Controllers\TableController;

Route::get('/', [Controller::class, 'datang']);
Route::get('/table', [TableController::class, 'table']);
Route::get('/data-table', [TableController::class, 'dataTable']);
